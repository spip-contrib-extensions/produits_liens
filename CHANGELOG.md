# Changelog

## [4.1.1] - 2022-09-15

### Added

- Retrait compatibilité SPIP3 car necessite rangs_auteurs

- Compatible avec SPIP 4.1 et PHP 8.1

- Ajout des fichiers Readme et Changelog

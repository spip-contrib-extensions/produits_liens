<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Pipeline afficher_complement_objet
 * afficher la liaison des produits
 * ajout de produit sur les fiches objet (avec autorisation dans le squelette)
 *
 * @param  $flux
 * @return
 */
function produits_liens_afficher_complement_objet($flux) {
	if (
		$type = $flux['args']['type']
		and $id = intval($flux['args']['id'])
	) {
		$contexte = ['objet' => $type, 'id_objet' => $id];
		$flux['data'] .= recuperer_fond('prive/objets/contenu/portfolio_produits', array_merge($_GET, $contexte));

		// afficher les objets liés sur la page du produit
		if ($type == 'produit') {
			$flux['data'] .= recuperer_fond(
				'prive/objets/liste/objets_lies_produits',
				[
					'id_produit' => $id,
					'titre' => 'Objets liés à ce produit',
					'statut' => ['prepa', 'prop'],
					'cacher_tri' => true,
					'nb' => 5
				],
				['ajax' => true]
			);
		}
	}
	return $flux;
}


/**
 * Configuration des contenus
 * @param array $flux
 * @return array
 */
function produits_liens_recuperer_fond($flux) {

	// surcharge la configuration du plugin produits
	if ($flux['args']['fond'] == 'formulaires/configurer_produits') {
		include_spip('inc/config');

		$flux['args']['contexte']['produits_objets'] = lire_config('produits/produits_liens/produits_objets');

		$objets_liens = recuperer_fond('formulaires/inc-configurer_produits_liens', $flux['args']['contexte']);
		$flux['data']['texte'] = str_replace('<!--extra-->', '<!--extra-->' . $objets_liens, $flux['data']['texte']);
	}
	return $flux;
}

function produits_liens_formulaire_traiter($flux) {

	// surcharge le traitement pour la configuration du plugin produits
	if ($flux['args']['form'] == 'configurer_produits') {
		include_spip('inc/config');
		$produits_objets = _request('produits_objets');

		ecrire_config('produits/produits_liens/produits_objets', $produits_objets);
	}
	return $flux;
}


/**
 * Compter les produits dans un objet
 *
 * @param array $flux
 * @return array
 */
function produits_liens_objet_compte_enfants($flux) {
	if (
		$objet = $flux['args']['objet']
		and $id = intval($flux['args']['id_objet'])
	) {
		// juste les publies ?
		if (array_key_exists('statut', $flux['args']) and ($flux['args']['statut'] == 'publie')) {
			$flux['data']['produit'] = sql_countsel('spip_produits AS D JOIN spip_produits_liens AS L ON D.id_produit=L.id_produit', 'L.objet=' . sql_quote($objet) . 'AND L.id_objet=' . intval($id) . " AND (D.statut='publie')");
		} else {
			$flux['data']['produit'] = sql_countsel('spip_produits AS D JOIN spip_produits_liens AS L ON D.id_produit=L.id_produit', 'L.objet=' . sql_quote($objet) . 'AND L.id_objet=' . intval($id) . " AND (D.statut='publie' OR D.statut='prepa')");
		}
	}
	return $flux;
}

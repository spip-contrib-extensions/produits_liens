<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//B
	'bouton_voir_produit'=>'Voir ce produit',
	
	//C
	'cfg_titre_parametrages' => 'Paramétrages',
	'champ_titre_label'=>'Titre',
	
	//D
	'des_produits'=>'Des produits',

	//E
	'erreur_aucun_produit'=>'Erreur : aucun produit',
		
	//I
	'id_produit_a_lier'=>'Identifiant :',
	'info_nb_produits' => '@nb@ produits',
	'info_1_produit' => '1 produit',
	'info_aucun_produit' => 'Aucun produit',
	
	//L
	'label_activer_liaison_produits_objets' => 'Lier des produits aux contenus suivants',
	'lien_ajouter_produit'   => 'Ajouter ce produit',
	'lier_ce_produit' =>'Lier ce produit',
	'lien_ordonner' =>'Ordonner les produits',
	'lien_retirer_produit'   => 'Retirer ce produit',
	'lien_retirer_tous_produits'  =>'Retirer tous ces produits',

	//N
	'nb_produits_attache_succes' => 'Les produits ont bien été liés',

	//P
	'produit_lie_succes'=>'Le produit a bien été lié',
	
	//T
	'titre_element'=>'Titre',
	'titre_page_configurer_produits_liens' => 'Configuration des liaisons de produits',
	'tous_les_produits' => 'Tous les produits',
		
	//U
	'un_produit'=>'Un produit',	

);


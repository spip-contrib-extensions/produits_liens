<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'produits_liens_description' => 'Lier et ordonner des produits sur les objets choisis dans la configuration du plugin produits. 
	Création de la table spip_produits_liens.',
	'produits_liens_nom' => 'Produits liens',
	'produits_liens_slogan' => 'Lier et ordonner des produits sur un objet déclaré.',
);
